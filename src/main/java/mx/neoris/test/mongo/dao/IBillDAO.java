package mx.neoris.test.mongo.dao;

import mx.neoris.test.mongo.model.FacturaBean;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Copyright: Copyright (c) 2017 Company: Neoris.<br>
 * <p>
 * Descripción: Interface DAO JDBC Template de aplicación [<b>MicroServicios Spring Boot</b>].
 * Clase creada el 20180412.
 *
 * @author #HUGO.MUNOZ [<a href="mailto:hugo.munoz@neoris.com">hugo.munoz@neoris.com</a>].
 * @version 1.0.0
 */
public interface IBillDAO extends PagingAndSortingRepository<FacturaBean, ObjectId> {

    @Query("{ 'ide_id' : ?0 }")
    FacturaBean findOneByIdeId(String ide_id);

}
