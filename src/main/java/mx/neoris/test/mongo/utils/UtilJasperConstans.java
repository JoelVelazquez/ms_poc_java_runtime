package mx.neoris.test.mongo.utils;

public final class UtilJasperConstans {

    /**
     * Constant
     * STR_WE_INF_JASPER
     */
    public static final String STR_WE_INF_JASPER = "jasper/";

    /**
     * Constant
     * STR_SUBREPORT_DIR
     */
    public static final String STR_SUBREPORT_DIR = "SUBREPORT_DIR";
    /**
     * Constant
     * STR_PATH_LOGO
     */
    public static final String STR_PATH_LOGO = "PATH_LOGO";

    /**
     * Constant
     * STR_IMG
     */
    public static final String STR_IMG = "img/";

    /**
     * Constant
     * JASPER_FACTURACION
     */
    public static final String JASPER_FACTURACION = "Facturacion.jasper";

    /**
     * Constant
     * JASPER_FACTURACION
     */
    public static final String JASPER_FACTURA = "Factura.jasper";

    /**
     * Constant
     * carpeta temporal sources
     */
    public static final String DIR_ZIP_TEMP = "zipTemp/";

    /**
     * completar path
     */
    public static final String COMPLETE_PATH = "/";
    /**
     * completar path
     */
    public static final String CLASS_PATH_OPEN_SHIFT = "/tmp/src/target/classes/";
}
