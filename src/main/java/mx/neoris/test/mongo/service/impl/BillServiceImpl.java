package mx.neoris.test.mongo.service.impl;

import mx.neoris.test.mongo.dao.IBillDAO;
import mx.neoris.test.mongo.model.FacturaBean;
import mx.neoris.test.mongo.service.IBillService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Copyright: Copyright (c) 2018 Company: Neoris.<br>
 * <p>
 * Descripción: Implemetación de Interface Service de aplicación [<b>MicroServicios Spring Boot</b>].
 * implementa {@link IBillService}.
 * Clase creada el 20180411.
 *
 * @author #HUGO.MUNOZ[<a       href   =   "   mailto:hugo.munoz   @   neoris.com   ">hugo.munoz@neoris.com</a>].
 * @version 1.0.0
 */
@Service("BillService")
public class BillServiceImpl implements IBillService {
    /**
     * Se crea interfaz para consumo de metodos
     */
    @Autowired
    private IBillDAO billDAO;

    /**
     * Metodo para recuperar Factura por ObjectId
     *
     * @param id {@link ObjectId}
     * @return {@link FacturaBean}
     */
    @Override
    public FacturaBean findById(ObjectId id) {
        return billDAO.findOne(id);
    }

    /**
     * Metodo para recuperar Factura por ObjectId
     *
     * @param ide_id {@link ObjectId}
     * @return {@link FacturaBean}
     */
    @Override
    public FacturaBean findByIdeId(String ide_id) {
        return billDAO.findOneByIdeId(ide_id);
    }

    /**
     * Metodo para salvar una factura
     *
     * @param facturaBean bean con formulario a salvar
     */
    @Override
    public void save(FacturaBean facturaBean) {
        billDAO.save(facturaBean);
    }

    /**
     * Metodo para eliminar una factura
     *
     * @param id identificador
     */
    @Override
    public void delete(ObjectId id) {
        billDAO.delete(id);
    }

    /**
     * Metodo para recuperar todas las facturas
     * paginadas
     *
     * @param pageable facturas paginadas
     */
    @Override
    public Page<FacturaBean> findAll(Pageable pageable) {
        return billDAO.findAll(pageable);
    }
}
