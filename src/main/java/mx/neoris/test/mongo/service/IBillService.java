package mx.neoris.test.mongo.service;

import mx.neoris.test.mongo.model.FacturaBean;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

/**
 * Copyright: Copyright (c) 2018 Company: Neoris.<br>
 * <p>
 * Descripción: Interface Service  para aplicativo [<b>MicroServicios Spring Boot</b>].
 * Clase creada el 20180411.
 *
 * @version 1.0.0
 */
@Component
public interface IBillService {
    /**
     * Metodo para recuperar Factura por ObjectId
     *
     * @param id {@link ObjectId}
     * @return {@link FacturaBean}
     */
    FacturaBean findById(ObjectId id);

    /**
     * Metodo para recuperar Factura por ObjectId
     *
     * @param ide_id {@link String}
     * @return {@link FacturaBean}
     */
    FacturaBean findByIdeId(String ide_id);

    /**
     * Metodo para salvar una factura
     *
     * @param facturaBean bean con formulario a salvar
     */
    void save(FacturaBean facturaBean);

    /**
     * Metodo para eliminar una factura
     *
     * @param id identificador
     */
    void delete(ObjectId id);

    /**
     * Metodo para recuperar todas las facturas
     * paginadas
     *
     * @param pageable facturas paginadas
     * @return {@link Page}
     */
    Page<FacturaBean> findAll(Pageable pageable);

}
